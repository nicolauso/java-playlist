import java.util.ArrayList;

public class Artist {
    private String firstName;
    private String lastName;

    public String getFullName() {
        return this.firstName + ' ' + this.lastName;
    }

    public String setFirstName(String newFirstName) {
       return this.firstName = newFirstName;
    }

    private String setLastName(String newLastName) {
        return this.lastName = newLastName;
    }

    public Artist(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }
}
