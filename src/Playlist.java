import java.util.ArrayList;

public class Playlist {

    private Music currentMusic;
    private ArrayList<Music> musicList = new ArrayList<>();
    public int totalDuration;

    public Playlist(Music currentMusic, ArrayList<Music> musicList) {
        this.currentMusic = currentMusic;
        this.musicList = musicList;
    }

    public Music getCurrentMusic() {
        return this.currentMusic;
    }

    public ArrayList<Music> getMusicList() {
        return this.musicList;
    }

    public void addMusic(Music music) {
        this.musicList.add(music);
    }


    public void removeMusic(int position) {
        this.musicList.remove(position);
    }


    public int getTotalDuration(ArrayList<Music> musicList) {
        for (Music music : musicList) {
            int musicDuration = music.getDuration();
            this.totalDuration = (this.totalDuration + musicDuration);
        }
        return this.totalDuration;
    }

    //  next() : passe à la musique suivante dans la liste et la définit comme musique en cours
    public String next() {
        int currentPosition = musicList.indexOf(currentMusic);

        if (musicList.indexOf(currentMusic) >=  musicList.size()) {
            currentMusic = musicList.get(currentPosition = 0);
            return currentMusic.getInfos();
        } else {
            currentMusic = musicList.get(currentPosition + 1);
            return currentMusic.getInfos();
        }

    }
}
