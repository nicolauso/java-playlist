//main() : ici tu créeras des instances des autres classes afin de tester leurs méthodes

import java.util.ArrayList;

public class Player {
    public static void main(String[] args) {

// Artist
        Artist rickyMartin = new Artist("Ricky", "Martin");
        Artist bandolero= new Artist("José", "Perez");
        Artist bandoleroTwo = new Artist("Carlos", "Perez");
        Artist tokioHotelOne = new Artist("Bill", "Kaulitz");
        Artist tokioHotelTwo = new Artist("Tom", "Kaulitz");


// Music
        ArrayList<Artist> artistSetOne = new ArrayList<>();
        artistSetOne.add(rickyMartin);
        Music music1 = new Music("La vida loca", 236, artistSetOne);
        String musiqueOneInfos =  music1.getInfos();



        ArrayList<Artist> artistSetTwo = new ArrayList<>();
        artistSetTwo.add(bandolero);
        artistSetTwo.add(bandoleroTwo);
        Music music2 = new Music("Paris Latino", 300, artistSetTwo);
        String musiqueTwoInfos = music2.getInfos();

        ArrayList<Artist> artistSetThree = new ArrayList<>();
        artistSetThree.add(tokioHotelOne);
        artistSetThree.add(tokioHotelTwo);
        Music music3 = new Music("Schrei", 4691, artistSetThree);
        String musiqueThreeInfos = music3.getInfos();
        //System.out.println(musiqueThreeInfos);

// Playlist
        Playlist BestPlaylistEver = new Playlist(null, new ArrayList<Music>());

        BestPlaylistEver.addMusic(music1);
        BestPlaylistEver.addMusic(music2);
        BestPlaylistEver.addMusic(music3);
        System.out.println(BestPlaylistEver.next());
        System.out.println(BestPlaylistEver.next());

    }
}
