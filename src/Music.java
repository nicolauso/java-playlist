import javax.naming.PartialResultException;
import java.util.ArrayList;

public class Music {

    private String title;
    private int duration;
    private ArrayList<Artist> artistSet = new ArrayList<>();
    private int minutes;
    private int secondes;

    public Music(String title, int duration, ArrayList<Artist> artistSet) {

        this.title = title;
        this.duration = duration;
        this.artistSet = artistSet;
    }

    public String getInfos() {
        String artistInfos = "";
        for(Artist artist : artistSet) {
            artistInfos = artistInfos + ' ' + artist.getFullName();
        }
        minutes = (this.duration / 60);
        secondes = this.duration % 60 ;

        return this.title + ' ' +  this.minutes + " " + "minutes" + " " + this.secondes + " " + "secondes." + ' ' + artistInfos;
    }

    public int getDuration() {
         return this.duration;
    }
}
